const serverURL = "https://noroff-komputer-store-api.herokuapp.com/"

//variables for the work button stuff
const workButtonElement = document.getElementById("workBtn");
const workCashText = document.getElementById("workCash");
const workWage = 100;

//variables for getting work money into bank
const bankButtonElement = document.getElementById("bankBtn");
const bankBalanceText = document.getElementById("balanceText");

//variables for making a loan
const loanButtonElement = document.getElementById("loanBtn");
const loanAmountText = document.getElementById("loanText");
const loanButtonGroup = document.getElementById("loanButtons");

//variables for where to put computer information
const listOfFeatures = document.getElementById("listOfFeatures");
const imageArea = document.getElementById("imageArea");
const descArea = document.getElementById("descArea");

//variables for buying
const buyButtonGroup = document.getElementById("buyButtonGroup");
const stockArea = document.getElementById("stockArea");

//objects to be manipulated
let workCash = 0;
let bankBalance = 0;
let loanAmount = 0;

//format currency
const currFormatNok = (money) => {
    return new Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' }).format(money);
}

//work related
const updateWorkCash = () => {
    workCashText.innerText = currFormatNok(workCash);
}

const doWork = () => {
    workCash = workCash + workWage;
    updateWorkCash();
}

//banking wage related
const updateBankBalance = () => {
    bankBalanceText.innerText = currFormatNok(bankBalance);
}

const transferMoneyToBank = () => {
    if (loanAmount !== 0) {
        if ((workCash * 0.1) >= loanAmount) {
            workCash = workCash - loanAmount;
            loanAmount = 0;
        } else {
            loanAmount = loanAmount - (workCash * 0.1);
            workCash = workCash - (workCash * 0.1);
        }
        if (loanAmount === 0) {
            button = document.getElementById("loanPayBtn");
            button.remove();
            loanButtonGroup.classList.remove("buttonGroup");
            loanButtonGroup.classList.add("buttonAlone");        
        }
    }
    bankBalance = bankBalance + workCash;
    workCash = 0;
    updateBankBalance();
    updateWorkCash();
    updateLoanAmount();
}

//making loan related
const updateLoanAmount = () => {
    loanAmountText.innerText = currFormatNok(loanAmount);
}

//some very nasty if loops
const makingLoanClick = () => {
    //returns out of function
    //to avoid excessive indentation
    if (loanAmount !== 0) {
        alert("You can't make a second loan");
        return;
    }
    if (bankBalance === 0) {
        alert("Broke people can't make loans")
        return;
    }

    let request = prompt("How much money do you want to borrow?");
    request = parseInt(request);
    if (isNaN(request)) {
        alert("Type a proper number");
    } else if (request === 0) {
        alert("Lending you 0 money makes no sense")
    } else if (request < 0) {
        alert("You can't make a negative loan")
    } else if (request > bankBalance * 2) {
        alert("Loan too high, max loan is double bank balance")
    } else {
        makingLoan(request);
    }
}

const makingLoan = (request) => {
    loanAmount = request;
    bankBalance = bankBalance + loanAmount;
    updateLoanAmount();
    updateBankBalance();
    loanButtonGroup.classList.remove("buttonAlone");
    loanButtonGroup.classList.add("buttonGroup");
    addButton("Pay back loan", loanButtonGroup, "loanPayBtn", payBackLoan);
}

const addButton = (text, group, id, func) => {
    const newDiv = document.createElement("button");
    newDiv.id = id;
    const newContent = document.createTextNode(text);
    newDiv.appendChild(newContent);
    group.appendChild(newDiv);
    newDiv.addEventListener("click", func);
}

const payBackLoan = () => {
    if (workCash === 0) {
        alert("No work pay!");
        return;
    }
    let payBack;
    if (workCash >= loanAmount) {
        payBack = loanAmount;
        workCash = workCash - payBack;
        loanAmount = 0;
    } else {
        payBack = workCash;
        loanAmount = loanAmount - payBack;
        workCash = 0;
    }
    updateLoanAmount();
    updateWorkCash();
    alert(`Paid back ${currFormatNok(payBack)}\nRemaining loan: ${currFormatNok(loanAmount)}`);
}

//runs the next function on each computer in the array
const addComputersToList = (computer) => {
    computer.forEach(x => addComputerToList(x));
    addSpecs(computer[0]);
}

//adds each computer to the dropdown menu
const addComputerToList = (computer) => {
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computerListElement.appendChild(computerElement);
}

//writes out list of specs
const addSpec = (spec) => {
    const specElement = document.createElement("li");
    specElement.appendChild(document.createTextNode(spec));
    listOfFeatures.appendChild(specElement);
}
/*
adds all relevant page elements for
the current computer
deletion of elements is done in another
function called delSpecs 
*/
const addSpecs = (computer) => {
    addImage(computer);
    addDesc(computer);
    addPrice(computer);
    addPriceBtn(computer);
    const specs = computer.specs
    specs.forEach(x => addSpec(x));
}

const addImage = (computer) => {
    const url = serverURL + computer.image
    putUpImage(url)
    /*
    timeout 0 to pop this operation
    fully off the call stack and onto
    the macrotask queue so it doesn't
    block the rest of the script.
    significantly speeds up loading images
    */
    setTimeout(() => {
        //check if link is valid
        fetch(url)
            .then (output => {
                if (!output.ok) {
                    replaceImage(url);
                }})
    }, 0);
}

//runs if image link is invalid
const replaceImage = (url) => {
    const type = findImageType(url);
    /*
    this will break if the url has .jpg or .png
    in it since string.replace() changes the
    first instance of any value
    */
    let newURL = ''
    if (type === "jpg") {
        newURL = url.replace(".jpg", ".png");
    } else if (type === "png") {
        newURL = url.replace(".png", ".jpg");
    } else {
        putUpImage(url);
    }
    //test the new image link
    //if it's still broken use the old one
    fetch(newURL)
        .then (output => {
            //url sanity check
            //this fixes the broken img
            //sometimes overwriting a valid img
            //presumably since this operation 
            //is pushed back to the callback queue
            if (output.ok && imageArea.children[0].src === url) {
                putUpImage(newURL);
            } else if (imageArea.children[0].src === url) {
                putUpImage(url);
            }})
}

const findImageType = (url) => {
    //to stop the while loop running forever
    //if the string has no period in it
    if (url.search(".") === -1) {
        return null;
    }
    let type = "";
    let i = url.length - 1;
    while (url[i] != ".") {
        type = url[i] + type;
        i--;
    }
    return type;
}

const putUpImage = (url) => {
    let imageElement = document.createElement("img");
    imageElement.src = url;
    imageElement.alt = "picture of computer";
    imageArea.textContent = '';
    imageArea.append(imageElement);
}

const addDesc = (computer) => {
    const desc = computer.description;
    const descElement = document.createElement("div");
    descElement.id = "computerDescription";
    descElement.appendChild(document.createTextNode(desc));
    descArea.appendChild(descElement);
}

const addPrice = (computer) => {
    const price = computer.price;
    const priceElement = document.createElement("div");
    priceElement.id = "computerPrice";
    priceElement.appendChild(document.createTextNode(currFormatNok(price)));
    priceArea.appendChild(priceElement);
}

const addPriceBtn = (computer) => {
    addButton("Buy Now", buyButtonGroup, "buyBtn", function(){
        buyComputer(computer)
    });
}

const buyComputer = (computer) => {
    if (computer.price > bankBalance) {
        alert("Not enough money")
    } else {
        bankBalance = bankBalance - computer.price;
        updateBankBalance();
        alert(`Congratulations! You're now the owner of the great computer called "${computer.title}"!`);
    }
}

//runs when a computer is selected
const handleComputerListChange = (computer) => {
    const selectedComputer = computers[computer.target.selectedIndex];
    delSpecs();
    addSpecs(selectedComputer);
}

//overwrites text fields with blank
const delSpecs = () => {
    listOfFeatures.textContent = '';
    descArea.textContent = '';
    priceArea.textContent = '';
    buyButtonGroup.textContent = '';
}

updateWorkCash();

updateBankBalance();

updateLoanAmount();

workButtonElement.addEventListener("click", doWork);

bankButtonElement.addEventListener("click", transferMoneyToBank);

loanButtonElement.addEventListener("click", makingLoanClick);

//fetch data stuff here
//starting by making an empty array
let computers = []
const computerListElement = document.getElementById("laptopSelect");

//fetches the array of computer objects
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToList(computers));

computerListElement.addEventListener("change", handleComputerListChange);