## Komputer Store
Fake computer store website that reads data from a web API and puts it on screen. Implements a little game with javascript to make money, take a loan and ultimately buy computers. No information is stored and site resets upon browser refresh.

## Dependencies
No dependencies, only uses vanilla HTML, CSS and JavaScript (ES6).

## Acknowledgements
All code written by me Flemming Møller Pedersen.
Use of Fetch API is based on the Drinks App example given to us.
